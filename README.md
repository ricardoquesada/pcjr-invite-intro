# Invite intro for the Flash Party 2018


## Compile et al.

Pre-requisites:

* Install [nasm](https://www.nasm.us/)
* Install [lz4](https://www.dropbox.com/sh/mwa5geyxgl9m24k/AACtCCyO5W1_3-1bI8YxPHLca)
* Install [Alinker](https://gitlab.com/ricardoquesada/alink)
* Install [DosBox-x](http://dosbox-x.com/)

```
$ make res && make run
```


## Video:

https://www.youtube.com/watch?v=hJpeZJ6dCE0


