.PHONY: res

TARGET_NAME = invitro.exe
TARGET = bin/${TARGET_NAME}
ASM = nasm
ASMFLAGS = -fobj -Wall
LD = alink
LDFLAGS = -oEXE -m

default: $(TARGET)
all: res default

OBJECTS = main.o detect_card.o pztimer.o lz4_8088.o zx7_8086.o intro.o utils.o

%.o: src/%.asm
	$(ASM) $(ASMFLAGS) $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	echo "Linking..."
	$(LD) $(OBJECTS) $(LDFLAGS) -o $@

clean:
	echo "Cleaning..."
	-rm -f *.o
	-rm -f bin/*.map

run: $(TARGET)
	echo "Running game..."
	dosbox-x -conf conf/dosbox-x_pcjr.conf -c "mount c bin/ && dir" -c "c:" -c ${TARGET_NAME}

x: $(TARGET)
	echo "Compressing game..."
	-upx -9 --8086 $(TARGET)

runx: x
	echo "Running game..."
	dosbox-x -conf conf/dosbox-x_pcjr.conf -c "mount c bin/ && dir" -c "c:" -c ${TARGET_NAME}

dist: x
	echo "Generating distribution .zip"
	-rm invitro.zip
	-rm invitro/invitro.exe
	cp bin/invitro.exe invitro/
	zip invitro.zip -r invitro

res:
	echo "Generating resources..."
	echo "Compressing music..."
	python3 tools/convert_vgm_to_pvm.py res/cumparchiptune.vgm
	mv res/cumparchiptune.pvm src/uctumi-song.pvm
	echo "Converting graphics..."
	#python3 ~/progs/pc-8088-misc/tools/convert_gfx_to_bios_format.py -g 10 -o src/flashparty.bin res/flashparty.data
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/pungas.raw res/pungas_logo_cga.png
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/invites.raw res/invites_you_to.png
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/flashparty.raw res/flashparty_320x200_invtro.png
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/dates.raw res/21-23-september.png
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/remote_entries.raw res/remote_entries.png
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/welcome.raw res/welcome.png
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/more_info.raw res/more_info.png
	python3 tools/convert_gfx_to_bios_format.py -g 4 -o res/url.raw res/url.png
	echo "Compressing graphics..."
	lz4 -9 -f res/pungas.raw src/pungas.raw.lz4
	lz4 -9 -f res/invites.raw src/invites.raw.lz4
	lz4 -9 -f res/flashparty.raw src/flashparty.raw.lz4
	lz4 -9 -f res/dates.raw src/dates.raw.lz4
	lz4 -9 -f res/remote_entries.raw src/remote_entries.raw.lz4
	lz4 -9 -f res/welcome.raw src/welcome.raw.lz4
	lz4 -9 -f res/more_info.raw src/more_info.raw.lz4
	lz4 -9 -f res/url.raw src/url.raw.lz4
	echo "Done"
